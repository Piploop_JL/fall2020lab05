//Jimmy Le, 1936415
package movies.importer;

import java.util.ArrayList;

public class RemoveDuplicates extends Processor{
	
	public RemoveDuplicates(String sauce, String destination) {
		super(sauce, destination, false);
	}
	
	public ArrayList<String> process(ArrayList<String> arr){
		ArrayList<String> asUnique = new ArrayList<String>();
		for(int i = 0; i < arr.size(); i++) {
			
			
			String temp = arr.get(i);
			if(!(asUnique.contains(temp))) {
				asUnique.add(temp);
			}

		}
		return asUnique;
	}

}
