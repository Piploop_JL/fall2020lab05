//Jimmy Le, 1936415	
package movies.importer;
import java.util.*;
public class LowercaseProcessor extends Processor{
	
	public LowercaseProcessor(String sauce, String destination){
		super(sauce, destination, true);
		
	}
	
	
	public ArrayList<String> process(ArrayList<String> arr){
		
		ArrayList<String> asLower = new ArrayList<String>();
		for(int i = 0; i < arr.size(); i++) {
			
			String temp = arr.get(i);
			temp = temp.toLowerCase();
			asLower.add(temp);
		}
		return asLower;
	}

}
